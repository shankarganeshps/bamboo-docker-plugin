package it.com.atlassian.bamboo.plugins.docker.pageobjects.cli;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.google.common.collect.Iterables;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.AdvancedOptionsElement;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.bamboo.pageobjects.utils.PageElementFunctions.binder;

public class DockerRunTaskComponent implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerRunTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Docker";

    public static final String DOCKER_COMMAND_OPTION_RUN = "run";

    public static final String DOCKER_COMMAND_OPTION = "commandOption";
    public static final String IMAGE = "image";
    public static final String COMMAND = "command";
    public static final String DETACH = "detach";
    public static final String NAME = "name";
    public static final String LINK = "link";
    public static final String ENV_VARS = "envVars";
    public static final String ADDITIONAL_ARGS = "additionalArgs";

    public static final String CONTAINER_PORT = "containerPort";
    public static final String HOST_PORT = "hostPort";
    public static final String CONTAINER_PORT_PREFIX = "containerPort_";
    public static final String HOST_PORT_PREFIX = "hostPort_";

    public static final String SERVICE_WAIT = "serviceWait";
    public static final String SERVICE_URL_PATTERN = "serviceUrlPattern";
    public static final String SERVICE_TIMEOUT = "serviceTimeout";

    public static final String WORK_DIR = "workDir";
    public static final String HOST_DIRECTORY = "hostDirectory";
    public static final String CONTAINER_DATA_VOLUME = "containerDataVolume";
    public static final String HOST_DIRECTORY_PREFIX = "hostDirectory_";
    public static final String CONTAINER_DATA_VOLUME_PREFIX = "containerDataVolume_";

    public static final String CFG_WORKING_SUB_DIRECTORY = "workingSubDirectory";
    public static final String CFG_ENVIRONMENT_VARIABLES = "environmentVariables";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = DOCKER_COMMAND_OPTION)
    private SelectElement dockerCommandField;

    @ElementBy(name = IMAGE)
    private TextElement imageField;

    @ElementBy(name = COMMAND)
    private TextElement commandField;

    @ElementBy(name = DETACH)
    private CheckboxElement detachField;

    @ElementBy(name = HOST_PORT)
    private TextElement hostPortField;

    @ElementBy(name = CONTAINER_PORT)
    private TextElement containerPortField;

    @ElementBy(className = "docker-plugin__ports-actions--add")
    private PageElement addPortLink;

    @ElementBy(name = NAME)
    private TextElement nameField;

    @ElementBy(name = LINK)
    private CheckboxElement linkField;

    @ElementBy(name = ENV_VARS)
    private TextElement envVarsField;

    @ElementBy(name = ADDITIONAL_ARGS)
    private TextElement additionalArgsField;

    @ElementBy(name = SERVICE_WAIT)
    private CheckboxElement serviceWaitField;

    @ElementBy(name = SERVICE_URL_PATTERN)
    private TextElement serviceUrlField;

    @ElementBy(name = SERVICE_TIMEOUT)
    private TextElement serviceTimeoutField;

    @ElementBy(name = WORK_DIR)
    private TextElement workDirField;

    @ElementBy(name = HOST_DIRECTORY)
    private TextElement hostDirectoryField;

    @ElementBy(name = CONTAINER_DATA_VOLUME)
    private TextElement containerDataVolumeField;

    @ElementBy(className = "docker-plugin__volumes-actions--add")
    private PageElement addVolumeLink;

    @ElementBy(name = CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @ElementBy(id = "advancedOptionsSection")
    private PageElement advancedOptions;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    @Inject
    protected PageBinder pageBinder;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        getAdvancedOptionsElement().expand();

        dockerCommandField.select(Options.value(DOCKER_COMMAND_OPTION_RUN));

        if (config.containsKey(IMAGE))
        {
            imageField.setText(config.get(IMAGE));
        }
        if (config.containsKey(COMMAND))
        {
            commandField.setText(config.get(COMMAND));
        }
        if (Boolean.valueOf(config.get(DETACH)))
        {
            detachField.select();

            if (config.containsKey(NAME))
            {
                nameField.setText(config.get(NAME));
            }

            for (String containerPortKey : Iterables.filter(config.keySet(), BambooPredicates.startsWith(CONTAINER_PORT_PREFIX)))
            {
                final String hostPortKey = HOST_PORT_PREFIX + containerPortKey.substring(CONTAINER_PORT_PREFIX.length());

                if (config.containsKey(hostPortKey))
                {
                    hostPortField.setText(config.get(hostPortKey));
                }
                containerPortField.setText(config.get(containerPortKey));

                addPortLink.click();
            }
        }
        if (Boolean.valueOf(config.get(SERVICE_WAIT)))
        {
            serviceWaitField.select();

            if (config.containsKey(SERVICE_URL_PATTERN))
            {
                serviceUrlField.setText(config.get(SERVICE_URL_PATTERN));
            }

            if (config.containsKey(SERVICE_TIMEOUT))
            {
                serviceTimeoutField.setText(config.get(SERVICE_TIMEOUT));
            }
        }
        if (Boolean.valueOf(config.get(LINK)))
        {
            linkField.select();
        }
        if (config.containsKey(ENV_VARS))
        {
            envVarsField.setText(config.get(ENV_VARS));
        }
        if (config.containsKey(WORK_DIR))
        {
            workDirField.setText(config.get(WORK_DIR));
        }
        for (String containerDataVolumeKey : Iterables.filter(config.keySet(), BambooPredicates.startsWith(CONTAINER_DATA_VOLUME_PREFIX)))
        {
            final String hostDirectoryKey = HOST_DIRECTORY_PREFIX + containerDataVolumeKey.substring(CONTAINER_DATA_VOLUME_PREFIX.length());

            if (config.containsKey(hostDirectoryKey))
            {
                hostDirectoryField.setText(config.get(hostDirectoryKey));
            }
            containerDataVolumeField.setText(config.get(containerDataVolumeKey));

            addVolumeLink.click();
        }
        if (config.containsKey(ADDITIONAL_ARGS))
        {
            additionalArgsField.setText(config.get(ADDITIONAL_ARGS));
        }
        if (config.containsKey(CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(CFG_ENVIRONMENT_VARIABLES));
        }
    }

    private AdvancedOptionsElement getAdvancedOptionsElement()
    {
        return binder(pageBinder, AdvancedOptionsElement.class).apply(advancedOptions);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}