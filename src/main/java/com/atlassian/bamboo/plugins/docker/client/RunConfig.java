package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Immutable
public class RunConfig
{
    private final Option<String> command;
    private final Option<String> containerName;
    private final ImmutableList<PortMapping> ports;
    private final ImmutableList<DataVolume> volumes;
    private final Option<String> workDir;
    private final boolean detach;
    private final ImmutableMap<String, String> links;
    private final ImmutableMap<String, String> env;
    private final Option<String> additionalArgs;

    private RunConfig(@NotNull final Builder builder)
    {
        this.command = builder.command;
        this.containerName = builder.containerName;
        this.ports = ImmutableList.copyOf(builder.ports);
        this.volumes = ImmutableList.copyOf(builder.volumes);
        this.workDir = builder.workDir;
        this.detach = builder.detach;
        this.links = ImmutableMap.copyOf(builder.links);
        this.env = ImmutableMap.copyOf(builder.env);
        this.additionalArgs = builder.additionalArgs;
    }

    @NotNull
    public Option<String> getCommand()
    {
        return command;
    }

    @NotNull
    public Option<String> getContainerName()
    {
        return containerName;
    }

    @NotNull
    public List<PortMapping> getPorts()
    {
        return ports;
    }

    @NotNull
    public List<DataVolume> getVolumes()
    {
        return volumes;
    }

    @NotNull
    public Option<String> getWorkDir()
    {
        return workDir;
    }

    public boolean isDetach()
    {
        return detach;
    }

    @NotNull
    public Map<String, String> getLinks()
    {
        return links;
    }

    @NotNull
    public Map<String, String> getEnv()
    {
        return env;
    }

    @NotNull
    public Option<String> getAdditionalArgs()
    {
        return additionalArgs;
    }

    @NotNull
    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Option<String> command = Option.none();
        private Option<String> containerName = Option.none();
        private List<PortMapping> ports = Collections.emptyList();
        private List<DataVolume> volumes = Collections.emptyList();
        private Option<String> workDir = Option.none();
        private boolean detach = false;
        private Map<String, String> links = Collections.emptyMap();
        private Map<String, String> env = Collections.emptyMap();
        private Option<String> additionalArgs = Option.none();

        private Builder()
        {
        }

        @NotNull
        public Builder command(@NotNull final String command)
        {
            this.command = Option.some(command);
            return this;
        }

        @NotNull
        public Builder containerName(@NotNull final String containerName)
        {
            this.containerName = Option.some(containerName);
            return this;
        }

        @NotNull
        public Builder ports(@NotNull final List<PortMapping> ports)
        {
            this.ports = ports;
            return this;
        }

        @NotNull
        public Builder volumes(@NotNull final List<DataVolume> volumes)
        {
            this.volumes = volumes;
            return this;
        }

        @NotNull
        public Builder workDir(@NotNull final String workDir)
        {
            this.workDir = Option.some(workDir);
            return this;
        }

        @NotNull
        public Builder detach(final boolean detach)
        {
            this.detach = detach;
            return this;
        }

        @NotNull
        public Builder links(@NotNull final Map<String, String> links)
        {
            this.links = links;
            return this;
        }

        @NotNull
        public Builder env(@NotNull final Map<String, String> env)
        {
            this.env = env;
            return this;
        }

        @NotNull
        public Builder additionalArgs(@NotNull final String additionalArgs)
        {
            this.additionalArgs = Option.some(additionalArgs);
            return this;
        }

        @NotNull
        public RunConfig build()
        {
            return new RunConfig(this);
        }
    }
}