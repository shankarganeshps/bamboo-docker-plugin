package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.fugue.Option;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;

@Immutable
public class AuthConfig
{
    private final Option<String> registryAddress;
    private final Option<String> username;
    private final Option<String> password;
    private final Option<String> email;

    private AuthConfig(@NotNull final Builder builder)
    {
        this.registryAddress = builder.registryAddress;
        this.username = builder.username;
        this.password = builder.password;
        this.email = builder.email;
    }

    public Option<String> getRegistryAddress()
    {
        return registryAddress;
    }

    @NotNull
    public Option<String> getUsername()
    {
        return username;
    }

    @NotNull
    public Option<String> getPassword()
    {
        return password;
    }

    @NotNull
    public Option<String> getEmail()
    {
        return email;
    }

    public boolean isAuthorisationProvided()
    {
        return !username.isEmpty();
    }

    @NotNull
    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Option<String> registryAddress = Option.none();
        private Option<String> username = Option.none();
        private Option<String> password = Option.none();
        private Option<String> email = Option.none();

        private Builder()
        {
        }

        @NotNull
        public Builder registryAddress(@Nullable final String registryAddress)
        {
            if (StringUtils.isNotBlank(registryAddress))
            {
                this.registryAddress = Option.some(registryAddress);
            }
            return this;
        }

        @NotNull
        public Builder username(@Nullable final String username)
        {
            if (StringUtils.isNotBlank(username))
            {
                this.username = Option.some(username);
            }
            return this;
        }

        @NotNull
        public Builder password(@Nullable final String password)
        {
            if (StringUtils.isNotBlank(password))
            {
                this.password = Option.some(password);
            }
            return this;
        }

        @NotNull
        public Builder email(@Nullable final String email)
        {
            if (StringUtils.isNotBlank(email))
            {
                this.email = Option.some(email);
            }
            return this;
        }

        @NotNull
        public AuthConfig build()
        {
            return new AuthConfig(this);
        }
    }
}