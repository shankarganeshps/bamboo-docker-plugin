[@s.textfield labelKey='docker.server' name='serverUrl' cssClass="long-field" required=true /]
[@s.textfield labelKey='docker.image' name='image' cssClass="long-field" required=true /]
[@s.textfield labelKey='docker.command' name='command' cssClass="long-field" /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(detach?has_content || link?has_content || environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='docker.name' name='name' descriptionKey='docker.name.description.deploy' cssClass="long-field" /]
    [@s.label labelKey='docker.ports' cssClass='field-value-normal-font' escape=false]
        [@s.param name='value']
            <div class="description">[@s.text name='docker.ports.description' /]</div>
            <ul class="ports-list">
                [#list portsIndices?sort as index]
                    <li class="ports-list-item" data-ports-id="${index}">[@portSnippet index /]</li>
                [/#list]
            </ul>
            <div class="aui-button ports-add">
                [@ww.text name="docker.ports.add"/]
            </div>
        [/@s.param]
    [/@s.label]
    [@s.checkbox labelKey='docker.service.wait' toggle='true' name='serviceWait' /]
    [@ui.bambooSection dependsOn='serviceWait' showOn='true']
        [@s.textfield labelKey='docker.service.url.pattern' name='serviceUrlPattern' cssClass="long-field" required=true /]
        [@s.textfield labelKey='docker.service.timeout' name="serviceTimeout" required=true /]
    [/@ui.bambooSection]
    [@s.textfield labelKey='docker.env' name='envVars' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]

[#macro portSnippet index=0]
    [@s.textfield name="hostPort_${index}" cssClass="text short-field" theme="inline" placeholderKey="docker.ports.placeholder.host" /]:
    [@s.textfield name="containerPort_${index}" cssClass="text short-field" theme="inline" placeholderKey="docker.ports.placeholder.container" /]
    <div class="aui-button aui-button-subtle ports-remove">
        <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
        [@ww.text name="global.buttons.remove" /]
    </div>
    [@s.hidden name="portError_${index}" showValidationErrors=true /]
[/#macro]

<script type="text/x-template" title="ports-list-item-template">
    [#assign portSnippetInst][@portSnippet 869576137068/][/#assign]
    <li class="ports-list-item" data-ports-id="{index}">${portSnippetInst?replace("869576137068", "{index}")}</li>
</script>

<script type="text/javascript">
    BAMBOO.DOCKER.DockerTaskConfiguration.init({
        addPortsSelector: ".ports-add",
        portsListSelector: ".ports-list",
        templates: {
            portsListItem: "ports-list-item-template"
        }
    });
</script>